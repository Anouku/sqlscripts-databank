USE modernways;

INSERT INTO huisdieren (
baasje,
naam,
leeftijd,
soort 
)
values
('Vincent','misty','6','hond'),
('Christiane ','Ming ','8','hond'),
('Esther ','Bientje ','6','kat'),
('Jommeke ','Flip ','75','papageaai'),
('Villads','Berto ','1','kat'),
('Bert ','Ming','7','hond'),
('Thaïs','Suerta ','2','hond');
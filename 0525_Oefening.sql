use modernways;
alter table huisdieren add column Bewerking char(20);

SET SQL_SAFE_UPDATES =0;
UPDATE huisdieren
set Bewerking = "X";

SELECT CONCAT (Naam,'',Bewerking) From huisdieren
where Soort = "hond";


SET SQL_SAFE_UPDATES =1;
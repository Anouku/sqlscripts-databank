USE ModernWays;
insert into Boeken (
   Familienaam,
   Titel,
   Verschijningsjaar,
   Categorie)
values
  ('?', 'Beowulf', '0975', 'Mythologie'),
 ('Ovidius', 'Metamorfosen', '8', 'Mythologie');
  
select Verschijningsjaar from Boeken
where Verschijningsjaar < 0976
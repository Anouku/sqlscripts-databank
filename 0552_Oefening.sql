use modernways;
ALTER TABLE Baasje
ADD COLUMN huisdieren INT,
ADD CONSTRAINT fk_Baasje_huisdieren
  FOREIGN KEY (huisdieren_Id)
  REFERENCES huisdieren(Id);
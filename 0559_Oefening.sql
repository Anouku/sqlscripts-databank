use modernways;
ALTER TABLE liedjes
ADD COLUMN artiesten_Id INT,
ADD CONSTRAINT fk_liedjes_artiesten
  FOREIGN KEY (artiesten_Id)
  REFERENCES artiesten(Id);